DROP DATABASE IF EXISTS database112233;

CREATE DATABASE IF NOT EXISTS database112233;

use database112233;



DROP TABLE IF EXISTS user;

create table user(
userno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
username varchar(50) NOT NULL,
userpassword varchar(20) NOT NULL,
useremail varchar(30) NOT NULL UNIQUE ,
firstname varchar(10) NOT NULL,
lastname varchar(10) NOT NULL
);

DROP TABLE IF EXISTS admin;

create table admin(
adminno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
adminusername varchar(50) NOT NULL,
adminpassword varchar(20) NOT NULL,
adminemail varchar(30) NOT NULL,
firstname varchar(10) NOT NULL,
lastname varchar(10) NOT NULL
);

DROP TABLE IF EXISTS article;

create table article(
articleno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
articletitle varchar(100) NOT NULL,
articlecontent varchar(2000) NOT NULL,
userno INT NOT NULL,
FOREIGN KEY (userno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE
);



DROP TABLE IF EXISTS images;

create table images(
imagesno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
imagespath varchar(2000) NOT NULL,
usersno INT NOT NULL,
FOREIGN KEY (usersno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE
);

DROP TABLE IF EXISTS follow;

create table follow(
followno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
followersusersno INT NOT NULL,
followingusersno INT NOT NULL,
FOREIGN KEY (followersusersno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY (followingusersno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE
);

DROP TABLE IF EXISTS comment;

create table comment(
commentno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
articleno INT NOT NULL,
usersno INT NOT NULL,
commentdate DATETIME,
content varchar(2000) NOT NULL,
FOREIGN KEY (articleno) REFERENCES article(articleno) ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY (usersno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE
);


DROP TABLE IF EXISTS likeac;

create table likeac(
likeacno INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
articleno INT NOT NULL,
usersno INT NOT NULL,
FOREIGN KEY (articleno) REFERENCES article(articleno) ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY (usersno) REFERENCES user(userno) ON DELETE RESTRICT ON UPDATE CASCADE
);


DROP TABLE IF EXISTS userActivetyLiveLog;

create table userActivetyLiveLog(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
useraction varchar(2000),
adminno INT NOT NULL,
FOREIGN KEY (adminno) REFERENCES admin(adminno) ON DELETE RESTRICT ON UPDATE CASCADE
);

DROP TABLE IF EXISTS userTableAction;

create table userTableAction(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
description varchar(2000)
);




INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("waihong","qwe","waihongsiew@gmail.com","waihong","siew");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("thefacebook","qwe","thefacebook@gmail.com","facebook","the");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("yousze","qwe","yousze@gmail.com","yousze","teo");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("XJB","qwe","xjb@gmail.com","xj","bi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("liang gou","qwe","gou@gmail.com","xas","biasd");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("nihao","qwe","xaasjb@gmail.com","xasdj","bsi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("bob","qwe","xjdddb@gmail.com","xxj","bwi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("alice","qwe","xjssb@gmail.com","xzxj","bsi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("michele","qwe","xjccb@gmail.com","xasdzxcj","bwi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("jay chow","qwe","xjxb@gmail.com","xzj","bsi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("jia le","qwe","xjcxb@gmail.com","lee","bwi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("jason","qwe","xzxzjb@gmail.com","xasdj","bei");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("NMB","qwe","xzxczxcjb@gmail.com","xsdj","bwi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("nife","qwe","xzxczasssxcjb@gmail.com","xxczj","bdi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("sabun","qwe","xjzxcb@gmail.com","xzcj","bsi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("chllie","qwe","xjzxczb@gmail.com","xzxj","bai");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("ffka","fas","asda@gmail.com","xzasdxj","baasdi");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("chllie1","qwe1","xjzxczb1@gmail.com","xzx1j","b1ai");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("chllie2","qwe2","xjzxczb2@gmail.com","xzx2j","b22ai");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("chllie3","qwe3","xjzxczb3@gmail.com","xz3xj","b3ai");
INSERT INTO user (username,userpassword,useremail,firstname,lastname) VALUES("chllie4","qwe4","xjzxczb4@gmail.com","xz4xj","b4ai");

INSERT INTO admin (adminusername , adminpassword , adminemail,firstname,lastname) VALUES("waihong", "qwe", "waihong@gmail.com", "waihong","siew");

INSERT INTO images(imagespath,usersno)VALUES("images/waihong.jpg","1");
INSERT INTO images(imagespath,usersno)VALUES("images/facebook.png","2");
INSERT INTO images(imagespath,usersno)VALUES("images/yousze.jpg","3");
INSERT INTO images(imagespath,usersno)VALUES("images/xjb.jpg","4");
INSERT INTO images(imagespath,usersno)VALUES("images/Admin.png","5");
INSERT INTO images(imagespath,usersno)VALUES("images/dogDesert.jpg","6");
INSERT INTO images(imagespath,usersno)VALUES("images/tempPenguines.jpg","7");
INSERT INTO images(imagespath,usersno)VALUES("images/waihongJellyfish.jpg","8");
INSERT INTO images(imagespath,usersno)VALUES("images/waihongKoala.jpg","9");
INSERT INTO images(imagespath,usersno)VALUES("images/waihongLighthouse.jpg","10");
INSERT INTO images(imagespath,usersno)VALUES("images/qqqaaaqqq.jpg","11");
INSERT INTO images(imagespath,usersno)VALUES("images/hogngai.jpg","12");
INSERT INTO images(imagespath,usersno)VALUES("images/qw.jpg","13");
INSERT INTO images(imagespath,usersno)VALUES("images/aaa.jpg","14");
INSERT INTO images(imagespath,usersno)VALUES("images/sss.jpg","15");
INSERT INTO images(imagespath,usersno)VALUES("images/zxxc.jpg","16");


INSERT INTO follow(followersusersno,followingusersno)VALUES(1,2);
INSERT INTO follow(followersusersno,followingusersno)VALUES(2,3);
INSERT INTO follow(followersusersno,followingusersno)VALUES(3,4);
INSERT INTO follow(followersusersno,followingusersno)VALUES(1,3);
INSERT INTO follow(followersusersno,followingusersno)VALUES(1,4);
INSERT INTO follow(followersusersno,followingusersno)VALUES(2,1);
INSERT INTO follow(followersusersno,followingusersno)VALUES(3,1);
INSERT INTO follow(followersusersno,followingusersno)VALUES(4,1);

INSERT INTO article(articletitle,articlecontent,userno)VALUES("THE FACEBOOK","hellowEveryOne","2");
INSERT INTO article(articletitle,articlecontent,userno)VALUES("WaiHong","Very best of luck to DKIT Volleyball Club against Trinity College in the league semi final today. Dundalk Institute of Technology DkIT Students' Union","1");
INSERT INTO article(articletitle,articlecontent,userno)VALUES("YouSze","Search and find 100+ cheap London hostels from ¡ê5. Don't miss the great prices, book now!","3");
INSERT INTO article(articletitle,articlecontent,userno)VALUES("XJB","Join us and become a bartender - Travel the world and work at the same time, anywhere you want to go. The experience of a lifetime is waiting for you.","4");


-- INSERT INTO likeac(articleno,usersno)VALUES(1,1);


-- INSERT INTO comment(content,articleno,usersno)VALUES("good",1,2);
-- INSERT INTO comment(content,articleno,usersno)VALUES("very good",2,1);
-- INSERT INTO comment(content,articleno,usersno)VALUES("where are you?",3,3);
-- INSERT INTO comment(content,articleno,usersno)VALUES("ihelo",4,4);
-- INSERT INTO comment(content,articleno,usersno)VALUES("hehe",4,2);
-- INSERT INTO comment(content,articleno,usersno)VALUES("(@.@)",3,1);
-- INSERT INTO comment(content,articleno,usersno)VALUES("wow",2,3);
-- INSERT INTO comment(content,articleno,usersno)VALUES("hahahahahaha",1,4);

