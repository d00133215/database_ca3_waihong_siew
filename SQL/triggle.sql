
use database112233;
DROP TRIGGER IF EXISTS trigger_DateOfUserCreated; 
DELIMITER // 
CREATE TRIGGER trigger_DateOfUserCreated after INSERT ON user 
FOR EACH ROW 
BEGIN
  INSERT INTO userTableAction (description) 
  VALUES 
  ((CONCAT("new user created on " , now() )));
END // 
DELIMITER ;

DROP TRIGGER IF EXISTS trigger_DateOfUserDeleted; 
DELIMITER // 
CREATE TRIGGER trigger_DateOfUserDeleted after DELETE on user 
FOR EACH ROW 
BEGIN
  INSERT INTO userTableAction (description) 
  VALUES 
  ((CONCAT("user delected on " , now() )));
END // 
DELIMITER ;

DROP TRIGGER IF EXISTS trigger_UserEditedAccount; 
DELIMITER // 
CREATE TRIGGER trigger_UserEditedAccount after update on user 
FOR EACH ROW 
BEGIN
  INSERT INTO userTableAction (description) 
  VALUES 
  ((CONCAT("user details updated on " , now() )));
END // 
DELIMITER ;