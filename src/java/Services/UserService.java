/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Class.article;
import Class.comment;
import Class.images;
import Class.user;
import Dao.articleDao;
import Dao.commentDao;
import Dao.followDao;
import Dao.imagesDao;
import Dao.likeacDao;
import Dao.userDao;
import Exception.DaoException;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author d00133215
 */
public class UserService {
    
    static Logger log = Logger.getLogger("global");
    
    public user login(String username, String password)
    {
        userDao dao = new userDao();
        user u = null;
        try 
        {
            userDao usTemp = new userDao();
            u = dao.findUserByUserNameAndPassword(username, password);
            log.info("user logging");
            
        } 
        catch (DaoException e) 
        {
            log.error("user login exception/error");
            e.printStackTrace();
        }
        return u;
    }
    
    public user userRegister(String fname, String lname, String email, String password){
        
        userDao dao = new userDao();
        user u = null;
        
        try {
               u = dao.createUser(fname,password,email,fname,lname);  
               log.info("createUser");
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return u;
    }
    
    public ArrayList<article> listAll(user us1){
        articleDao dao = new articleDao();
        ArrayList<article> arcticleList = null;
        try {
               arcticleList = dao.listAllArticle();     
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return arcticleList;
    }
    public ArrayList<article> listAll(){
        articleDao dao = new articleDao();
        ArrayList<article> arcticleList = null;
        try {
               arcticleList = dao.listAllArticle();     
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return arcticleList;
    }
    public article createArticle(user x, String contain){
        article ar = null;
        articleDao dao = new articleDao();
        try {
               ar = dao.createArticle(x.getFirstName(), contain, x.getUserid());   
               log.info("createArticle: "+ x.getUserEmail());
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return ar;
    }
    public user editUser(user old,String fname,String lname,String password){
        user newUser = old;
        userDao dao = new userDao();
        try {
               newUser = dao.editDetail(old,fname,lname,password);  
               log.info(newUser.getUserName()+" user editing details");
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return newUser;
    }
    
    public String createImg(user x, String path){
        String newImgPath = null;
        imagesDao dao = new imagesDao();
        try {
               newImgPath = dao.createImageForUser(x,path);
               log.info(x.getUserName()+" user uploaded images");
            } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return newImgPath;
    }
    
    public String getImages(user x){
        String images = null;
                imagesDao dao =new imagesDao();
                try {
                images = dao.getImagePath(x.getUserid());
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return images;
    }
    
    public String getImagesById(int x){
        String images = null;
                imagesDao dao =new imagesDao();
                try {
                images = dao.getImagePath(x);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return images;
    }
    
    public user userProfile(int userId){
        user us = null;
                userDao dao =new userDao();
                try {
                us = dao.fineUserById(userId);
                log.info(us.getUserName()+" user view profile");
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
    return us;
    }
    
    public boolean follwing(int a, int b){
        boolean yn = false;
        followDao dao = new followDao();
         try {
                yn = dao.followingUser(a,b);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return yn;
    }
    
    public boolean unfollwing(int a, int b){
        boolean yn = false;
        followDao dao = new followDao();
         try {
                yn = dao.UnFollowingUser(a,b);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return yn;
    }
    
    public boolean checkFollow(int a, int b){
         boolean yn = false;
         followDao dao = new followDao();
         try {
                yn = dao.checkfollowed(a,b);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return yn;
    }
    
    public ArrayList<user> listAllFollwer(user u){
        ArrayList<user> userList= null;
        followDao dao = new followDao();
        try {
                userList = dao.followerList(u);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return userList;
    }
     public ArrayList<user> listAllFollwing(user u){
        ArrayList<user> userList= null;
        followDao dao = new followDao();
        try {
                userList = dao.followingList(u);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return userList;
    }
      public ArrayList<user> seaching(String w){
        ArrayList<user> userList= null;
        userDao dao = new userDao();
        try {
                userList = dao.findUserByName(w);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return userList;
    }
      
      public boolean likeAnPost(int actNum, user userNum){
      boolean yn = false;
      likeacDao dao = new likeacDao();
        try {
                yn = dao.likePost(actNum,userNum);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
      
     public article fineAcById(int actNum){
      article yn = null;
      articleDao dao = new articleDao();
        try {
                yn = dao.fineByID(actNum);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
     
     public ArrayList<comment> listAllcomment(article ac){
      ArrayList<comment> yn = new ArrayList<comment>();
      commentDao dao = new commentDao();
        try {
                yn = dao.ListAllCommentByArticleID(ac);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
     
      public boolean checkLike(int arid, user b){
         boolean yn = false;
         likeacDao dao = new likeacDao();
         try {
                yn = dao.ckLike(arid,b);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
        return yn;
    }
      
     
  public boolean UnlikeAnPost(int actNum, user userNum){
      boolean yn = false;
      likeacDao dao = new likeacDao();
        try {
                yn = dao.unlikePost(actNum,userNum);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
     
      public boolean createComment(int actNum, user userNum, String content){
      boolean yn = false;
      commentDao dao = new commentDao();
        try {
                yn = dao.createCommentinAr(actNum,userNum,content);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
    public boolean deleteAr(int actNum){
      boolean yn = false;
      articleDao dao = new articleDao();
        try {
                yn = dao.deleteArticlebyId(actNum);
                } 
            catch (Exception e) { 
                e.printStackTrace();
            }
      return yn;
      }
    
     public ArrayList<user> listAllUser(){
        userDao dao = new userDao();
        ArrayList<user> u = null;
        try 
        {
            u = dao.listAllUser();
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
            
}
