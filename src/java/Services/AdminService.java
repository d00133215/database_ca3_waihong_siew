/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Class.admin;
import Class.user;
import Dao.Dao;
import Dao.adminDao;
import Dao.articleDao;
import Dao.userDao;
import Exception.DaoException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author d00133215
 */
public class AdminService {
    
         
    	
    public admin login(String username, String password) throws DaoException, SQLException{
        adminDao dao = new adminDao();
        admin u = null;

        Connection con = null;  
        
        
        con = getConnection();
        String s1 = "{CALL listAllTheUserActivety(?,?)}";
    	CallableStatement cs = con.prepareCall(s1);
        cs.setInt(1, 1);
    	cs.setString(2, "admin loged in");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
        try 
        {
            u = dao.fineAdminByUserNameAndPassword(username, password);
            
        } 
        catch (DaoException e) 
        {
            
            e.printStackTrace();
        }
        return u;
    }
    public ArrayList<user> listAllUser() throws SQLException{
        userDao dao = new userDao();
        ArrayList<user> u = null;
        try 
        {
            u = dao.listAllUser();
            
            Connection con = null;    
        con = getConnection();
        String s1 = "{CALL listAllTheUserActivety(?,?)}";
    	CallableStatement cs = con.prepareCall(s1);
        cs.setInt(1, 1);
    	cs.setString(2, "admin listAlluser in");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
            
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return u;
    }
    public boolean deleteArticle(String content) throws SQLException{
        articleDao dao = new articleDao();
        boolean yn = false;
        try 
        {
           yn = dao.deleteArticle(content);
           
           Connection con = null;    
        con = getConnection();
        String s1 = "{CALL listAllTheUserActivety(?,?)}";
    	CallableStatement cs = con.prepareCall(s1);
        cs.setInt(1, 1);
    	cs.setString(2, "admin deleted article in");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
           
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return yn;
    }
    public boolean deleteUser(String email) throws SQLException{
        userDao dao = new userDao();
        boolean yn = false;
        
        try 
        {
           dao.deleteUser(email);
           Connection con = null;    
        con = getConnection();
        String s1 = "{CALL listAllTheUserActivety(?,?)}";
    	CallableStatement cs = con.prepareCall(s1);
        cs.setInt(1, 1);
    	cs.setString(2, "admin delete user in");        
        cs.execute();
        ResultSet rs = cs.getResultSet();
        } 
        catch (DaoException e) 
        {
            e.printStackTrace();
        }
        return yn;
    }
    
    
    
    
     public Connection getConnection() throws DaoException {

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/database112233";
        String username = "root";
        String password = "";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex1) {
            System.out.println("Failed to find driver class " + ex1.getMessage());
            System.exit(1);
        } catch (SQLException ex2) {
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2);
        }
        return con;
    }
}
