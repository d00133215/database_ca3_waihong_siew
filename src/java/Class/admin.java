/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Class;

/**
 *
 * @author d00133215
 */
public class admin {

    public int adminId;
    public String amdinName;
    public String adminPassword;
    public String adminEmail;
    public String firstName;
    public String lastName;

    public admin() {
    }

    public admin(int adminId, String amdinName, String adminPassword, String adminEmail, String firstName, String lastName) {
        this.adminId = adminId;
        this.amdinName = amdinName;
        this.adminPassword = adminPassword;
        this.adminEmail = adminEmail;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getAdminId() {
        return adminId;
    }

    public String getAmdinName() {
        return amdinName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public void setAmdinName(String amdinName) {
        this.amdinName = amdinName;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "admin{" + "adminId=" + adminId + ", amdinName=" + amdinName + ", adminPassword=" + adminPassword + ", adminEmail=" + adminEmail + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.adminId;
        hash = 59 * hash + (this.amdinName != null ? this.amdinName.hashCode() : 0);
        hash = 59 * hash + (this.adminPassword != null ? this.adminPassword.hashCode() : 0);
        hash = 59 * hash + (this.adminEmail != null ? this.adminEmail.hashCode() : 0);
        hash = 59 * hash + (this.firstName != null ? this.firstName.hashCode() : 0);
        hash = 59 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final admin other = (admin) obj;
        if (this.adminId != other.adminId) {
            return false;
        }
        if ((this.amdinName == null) ? (other.amdinName != null) : !this.amdinName.equals(other.amdinName)) {
            return false;
        }
        if ((this.adminPassword == null) ? (other.adminPassword != null) : !this.adminPassword.equals(other.adminPassword)) {
            return false;
        }
        if ((this.adminEmail == null) ? (other.adminEmail != null) : !this.adminEmail.equals(other.adminEmail)) {
            return false;
        }
        if ((this.firstName == null) ? (other.firstName != null) : !this.firstName.equals(other.firstName)) {
            return false;
        }
        if ((this.lastName == null) ? (other.lastName != null) : !this.lastName.equals(other.lastName)) {
            return false;
        }
        return true;
    }
    
    
}
