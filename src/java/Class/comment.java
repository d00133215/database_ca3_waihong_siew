/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Class;

/**
 *
 * @author waihong
 */
public class comment {
    private int cNo;
    private int arNo;
    private int uNo;
    private String commentD;
    private String content;

    public comment() {
    }

    public comment(int cNo, int arNo, int uNo, String commentD, String content) {
        this.cNo = cNo;
        this.arNo = arNo;
        this.uNo = uNo;
        this.commentD = commentD;
        this.content = content;
    }

    public comment(int cNo, int arNo, int uNo, String content) {
        this.cNo = cNo;
        this.arNo = arNo;
        this.uNo = uNo;
        this.content = content;
    }

    public int getcNo() {
        return cNo;
    }

    public void setcNo(int cNo) {
        this.cNo = cNo;
    }

    public int getArNo() {
        return arNo;
    }

    public void setArNo(int arNo) {
        this.arNo = arNo;
    }

    public int getuNo() {
        return uNo;
    }

    public void setuNo(int uNo) {
        this.uNo = uNo;
    }

    public String getCommentD() {
        return commentD;
    }

    public void setCommentD(String commentD) {
        this.commentD = commentD;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "comment{" + "cNo=" + cNo + ", arNo=" + arNo + ", uNo=" + uNo + ", commentD=" + commentD + ", content=" + content + '}';
    }
    
    
}
