/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Class;

/**
 *
 * @author waihong
 */
public class images {
    public int noImages;
    public String imagesPath;
    public int userno;

    public images(int noImages, String imagesPath, int userno) {
        this.noImages = noImages;
        this.imagesPath = imagesPath;
        this.userno = userno;
    }

    public images() {
    }
    
    

    public int getNoImages() {
        return noImages;
    }

    public void setNoImages(int noImages) {
        this.noImages = noImages;
    }

    public String getImagesPath() {
        return imagesPath;
    }

    public void setImagesPath(String imagesPath) {
        this.imagesPath = imagesPath;
    }

    public int getUserno() {
        return userno;
    }

    public void setUserno(int userno) {
        this.userno = userno;
    }
    
    
}
