/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Class;

/**
 *
 * @author d00133215
 */
public class article {
    public int articleId;
    public String title;
    public String content;
    public int userCreatedId;

    public article() {
    }

    public article(int articleId, String title, String content, int userCreatedId) {
        this.articleId = articleId;
        this.title = title;
        this.content = content;
        this.userCreatedId=userCreatedId;
    }

    public int getUserCreatedId() {
        return userCreatedId;
    }

    public void setUserCreatedId(int userCreatedId) {
        this.userCreatedId = userCreatedId;
    }

    
    public int getArticleId() {
        return articleId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "article{" + "articleId=" + articleId + ", title=" + title + ", content=" + content + ", userCreatedId=" + userCreatedId + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.articleId;
        hash = 43 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 43 * hash + (this.content != null ? this.content.hashCode() : 0);
        hash = 43 * hash + this.userCreatedId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final article other = (article) obj;
        if (this.articleId != other.articleId) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.content == null) ? (other.content != null) : !this.content.equals(other.content)) {
            return false;
        }
        if (this.userCreatedId != other.userCreatedId) {
            return false;
        }
        return true;
    }

    
    
    
}
