/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Exception;

import java.sql.SQLException;
/**
 *
 * @author d00133215
 */

public class DaoException extends SQLException {

    public DaoException() {
    }

    public DaoException(String aMessage) {
        super(aMessage);
    }
}