/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Class.article;
import Class.user;
import Exception.DaoException;
import Interface.articleDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author waihong
 */

    public class imagesDao extends Dao {
        
        public String createImageForUser(user x, String path) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            con = getConnection();
            String query = "select * from images where usersno = ?;";
            ps = con.prepareStatement(query);
            ps.setInt(1, x.getUserid());
            
            rs=ps.executeQuery();
            
            if(rs.next()){
            con = getConnection();
            query = "delete from images where usersno = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, x.getUserid());
            ps.executeUpdate();
            }
            
            con = getConnection();
            query = "INSERT INTO images(imagespath,usersno)VALUES(?,?);";
            ps = con.prepareStatement(query);
            ps.setString(1, path);
            ps.setInt(2,x.getUserid());
            ps.executeUpdate();
            u=path;
            } catch (SQLException e) {
            throw new DaoException("createImageForUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createImageForUser(): " + e.getMessage());
            } }
        return u;
        }
        
        public String getImagePath(user x) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            con = getConnection();
            String query = "SELECT imagespath FROM images WHERE usersno = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,x.getUserid());

            rs = ps.executeQuery();
            if (rs.next()) {
            u =  rs.getString("imagespath");
            }
            } catch (SQLException e) {
            throw new DaoException("getImagePath(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("getImagePath(): " + e.getMessage());
            } }
        return u;
       
}
        public String getImagePath(int number) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            con = getConnection();
            String query = "SELECT imagespath FROM images WHERE usersno = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,number);

            rs = ps.executeQuery();
            if (rs.next()) {
            u =  rs.getString("imagespath");
            }
            } catch (SQLException e) {
            throw new DaoException("getImagePath(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("getImagePath(): " + e.getMessage());
            } }
        return u;
        }
    }
