/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Class.admin;
import Class.user;
import Exception.DaoException;
import Interface.adminDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author d00133215
 */
public class adminDao extends Dao implements adminDaoInterface{
    
    public admin fineAdminByUserNameAndPassword(String userEmail, String passsword) throws DaoException {
             Connection con = null;
             PreparedStatement ps = null;
             ResultSet rs = null; 
             admin u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM admin WHERE adminemail = ? AND adminpassword = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, userEmail);
            ps.setString(2, passsword);

            rs = ps.executeQuery();

            if (rs.next()) {
                int Id = rs.getInt("adminno");
                String adminUsersName = rs.getString("adminusername");
                String adminPassword = rs.getString("adminpassword");
                String email = rs.getString("adminemail");
                String FirstName = rs.getString("firstname");
                String LastName = rs.getString("lastname");
                u = new admin(Id, adminUsersName, adminPassword, email ,FirstName, LastName);
            }
            else{
                System.out.println("unable to login");
            }

        } catch (SQLException e) {
            throw new DaoException("fineAdminByUserName(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("fineAdminByUserName(): " + e.getMessage());
            }
        }
        return u ;
    }
   
    //only for testing
    public static void main(String [] args) throws DaoException{
        adminDao  dao = new adminDao();
        
    }
   
}

