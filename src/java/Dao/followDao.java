/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Class.user;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author waihong
 */
public class followDao extends Dao{
    public boolean followingUser(int a, int b) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();

            String command = "INSERT INTO follow (followersusersno,followingusersno) VALUES(?, ?)";
            ps = con.prepareStatement(command);
            ps.setInt(1, a);
            ps.setInt(2, b);
   ;
            ps.executeUpdate();
                  
            System.out.println("followed");
              yn=true;
            
        } catch (SQLException e) {
            throw new DaoException("createFoll(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createFoll(): " + e.getMessage());
            }
        }
        
        return yn;
    }
    public boolean UnFollowingUser(int a, int b) throws DaoException{
        boolean yn = true;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();

            String command = "DELETE FROM follow WHERE followersusersno=? AND followingusersno=?";
            ps = con.prepareStatement(command);
            ps.setInt(1, a);
            ps.setInt(2, b);
   
            ps.executeUpdate();
                  
            System.out.println("Unfollowed");
              yn=false;
            
        } catch (SQLException e) {
            throw new DaoException("Unfollowed(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("Unfollowed(): " + e.getMessage());
            }
        }
        return yn;
    }
    
    public boolean checkfollowed(int a, int b) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();

            String command = "Select * from follow where followersusersno=? and followingusersno=?";
            ps = con.prepareStatement(command);
            ps.setInt(1, a);
            ps.setInt(2, b);
   ;
            rs = ps.executeQuery();
            if (rs.next()) {
            yn =  true;
            }
            
        } catch (SQLException e) {
            throw new DaoException("createFoll(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createFoll(): " + e.getMessage());
            }
        }
        
        return yn;
    }
    
    public ArrayList<user> followerList(user u) throws DaoException{
        ArrayList<user> tempList = new ArrayList<user>();;
        ArrayList<String> numberTemp = null;
         Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            String command = "Select * from follow where followersusersno=? ";
            ps = con.prepareStatement(command);
            ps.setInt(1, u.getUserid());
            rs = ps.executeQuery();
            userDao uDao = new userDao();
            while (rs.next()) {
            int aa =rs.getInt("followingusersno");
            user tempUser = uDao.fineUserById(aa);
            tempList.add(tempUser);
            }
            
        } catch (SQLException e) {
            throw new DaoException("createFoll(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createFoll(): " + e.getMessage());
            }
        }
        
        return tempList;
    }
    public ArrayList<user> followingList(user u) throws DaoException{
        ArrayList<user> tempList = new ArrayList<user>();;
        ArrayList<String> numberTemp = null;
         Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            String command = "Select * from follow where followingusersno=? ";
            ps = con.prepareStatement(command);
            ps.setInt(1, u.getUserid());
            rs = ps.executeQuery();
            userDao uDao = new userDao();
            while (rs.next()) {
            int aa =rs.getInt("followersusersno");
            user tempUser = uDao.fineUserById(aa);
            tempList.add(tempUser);
            }
            
        } catch (SQLException e) {
            throw new DaoException("createFoll(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createFoll(): " + e.getMessage());
            }
        }
        
        return tempList;
    }
    
    
    public static void main(String []args) throws DaoException{
        followDao ff= new followDao();
     //   ArrayList<user> tt =  ff.followerList(1);
     //   System.out.println(tt.get(0));
    }
}
