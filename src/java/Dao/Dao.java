/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

/**
 *
 * @author d00133215
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Exception.DaoException;
import javax.activation.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Dao 
{
    private javax.sql.DataSource datasource;
    
    // Constructor for use by pooled connections
    public Dao()
    {
        Connection con = null;
        String DATASOURCE_CONTEXT = "jdbc/database112233";
        try {
            Context initialContext = new InitialContext();
            javax.sql.DataSource ds = (javax.sql.DataSource)initialContext.lookup("java:comp/env/" + DATASOURCE_CONTEXT);
            if(ds != null){
                datasource = ds;
            }
            else{
		System.out.println(("Failed to lookup datasource."));
            }
        }catch (NamingException ex ){
            System.out.println("Cannot get connection: " + ex);
        }
    }
    
    
    // Pooled version of getConnection
    public Connection getConnection() throws DaoException
    {
        Connection conn = null;
        try{
            if (datasource != null) {
                conn = datasource.getConnection();
            }else {
                System.out.println(("Failed to lookup datasource."));
            }
        }catch (SQLException ex2){
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2); // Abnormal termination
        }
        return conn;
    }

    public void freeConnection(Connection con) throws DaoException 
    {
        try 
        {
            if (con != null) 
            {
                con.close();
                con = null;
            }
        } 
        catch (SQLException e) 
        {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }   
}
/*
public class Dao {
    public Connection getConnection() throws DaoException {

        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/database112233";
        String username = "root";
        String password = "";
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex1) {
            System.out.println("Failed to find driver class " + ex1.getMessage());
            System.exit(1);
        } catch (SQLException ex2) {
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2);
        }
        return con;
    }

    public void freeConnection(Connection con) throws DaoException {
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }
}
*/
