/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Class.article;
import Class.user;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author waihong
 */
public class likeacDao extends Dao{
    

            
  public boolean ckLike(int number, user us) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            
            con = getConnection();
            String query = "SELECT * from likeac where articleno=? AND usersno = ?;";
            ps = con.prepareStatement(query);;
            ps.setInt(1,number);
            ps.setInt(2,us.getUserid());
            ps.executeQuery();
            rs = ps.executeQuery();
            if (rs.next()) {
              yn = true;
            }
            } catch (SQLException e) {
            throw new DaoException("createImageForUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createImageForUser(): " + e.getMessage());
            } 
            return yn;
       
    }
  }
  
  
          
    public boolean unlikePost(int number, user us) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            
            con = getConnection();
            String query = "DELETE from likeac WHERE articleno = ? AND usersno = ?";
            ps = con.prepareStatement(query);;
            ps.setInt(1,number);
            ps.setInt(2,us.getUserid());
            ps.executeUpdate();
            yn = true;
            } catch (SQLException e) {
            throw new DaoException("createImageForUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createImageForUser(): " + e.getMessage());
            } 
            return yn;
       
    }
    }
    
    public boolean likePost(int number, user us) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        String u = null;
        try {
            
            con = getConnection();
            String query = "INSERT INTO likeac(articleno,usersno)VALUES(?,?);";
            ps = con.prepareStatement(query);;
            ps.setInt(1,number);
            ps.setInt(2,us.getUserid());
            ps.executeUpdate();
            yn = true;
            } catch (SQLException e) {
            throw new DaoException("createImageForUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createImageForUser(): " + e.getMessage());
            } 
            return yn;
       
    }
    }
    
    public int displayLikeNumber(int articleNumber) throws DaoException{
        
        
        int num = 0;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        article u = null;
        try {
            con = getConnection();
            String query = "SELECT * FROM likeac WHERE articleno = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,articleNumber);

            rs = ps.executeQuery();
            while (rs.next()) {
               num = num +1;
            }
            
            
            
        } catch (SQLException e) {
            throw new DaoException("articleCreate(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("articleCreate(): " + e.getMessage());
            }
        }
        
        return num;
    }
}
