/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Class.article;
import Class.comment;
import Class.user;
import Exception.DaoException;
import Interface.articleDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author d00133215
 */
public class articleDao extends Dao implements articleDaoInterface{
    
    public article createArticle(String articleTitle,String articleContent,int userNo) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        article u = null;
        try {
            con = getConnection();
            String query = "SELECT articlecontent FROM article WHERE articlecontent = ?";
            ps = con.prepareStatement(query);
            ps.setString(1,articleContent);

            rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("article content " + articleContent + " already exists");
            }
            
            String command = "INSERT INTO article (articletitle  , articlecontent  , userno ) VALUES(?, ?, ?)";
            ps = con.prepareStatement(command);
            ps.setString(1, articleTitle);
            ps.setString(2, articleContent);
            ps.setInt(3, userNo);
            ps.executeUpdate();
                  
            System.out.println("article created");
            String command2 = "select * from article where articletitle = ?";
            ps2 = con.prepareStatement(command2);
            ps2.setString(1, articleTitle);
           
            rs = ps2.executeQuery();
            if (rs.next()) {
            int arcId = rs.getInt("articleno");
            u = new article(arcId,articleTitle,articleContent,userNo);
            }
            
        } catch (SQLException e) {
            throw new DaoException("articleCreate(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("articleCreate(): " + e.getMessage());
            }
        }
        return u;
    }
    
    public ArrayList<article> listAllArticle() throws DaoException{
        Connection con = null;
            ArrayList<article> arc = new ArrayList<article>();
             PreparedStatement ps = null;
             ResultSet rs = null; 
             article u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM article ";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
                int arcno = rs.getInt("articleno");
                String arctitile = rs.getString("articletitle");
                String arccontent = rs.getString("articlecontent");
                int userno = rs.getInt("userno");
                u = new article(arcno, arctitile, arccontent, userno);
                arc.add(u);
            }

        } catch (SQLException e) {
            throw new DaoException("listAllArticle(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("listAllArticle(): " + e.getMessage());
            }
        }
        return arc ;
    }
    
    public void editArticleTitle(String name ,String newName) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        try{
            con = getConnection();
            String query = "SELECT * FROM article WHERE articletitle=?";
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                int number = rs.getInt("articleno");
                String query2 = "UPDATE article SET articletitle=? WHERE articleno=?";
                ps2 = con.prepareStatement(query2);
                ps2.setString(1, newName);
                ps2.setInt(2, number);
                ps2.executeUpdate();
                System.out.println("article title changed");
            }
            else{
                System.out.println("article  " + name + " doesn't exists");
            }
        }
        catch (SQLException e) {
            throw new DaoException("editArticleTitle(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("editArticleTitle(): " + e.getMessage());
            }
        }
    }
    
    public boolean deleteArticle(String name) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try{
            con = getConnection();
            String query = "SELECT * FROM article WHERE articlecontent=?";
            ps = con.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if(rs.next()){
                       // likeacDao dao1 = new likeacDao();
                       // dao1.
                        int idid = rs.getInt("articleno");
                        String query2 = "DELETE FROM article WHERE articleno=?";
                        ps2=con.prepareStatement(query2);
                        ps2.setInt(1, idid);
                        ps2.executeUpdate();
                        System.out.println("delete successed");
                        return false;
            }
            else{
                System.out.println("the article that you inserted doesn't exist");
            }
        }
        catch (SQLException e) {
            throw new DaoException("deleteArticle(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteArticle(): " + e.getMessage());
            }
        }
        return true;
    }
    
    public boolean deleteArticleByUserId(int id) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        likeacDao lkDao = new likeacDao();
        commentDao coDao = new commentDao();
        try{
            
            con = getConnection();
            String query = "SELECT * FROM article WHERE userno=?";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
                       // likeacDao dao1 = new likeacDao();
                       // dao1.
                        int idid = rs.getInt("articleno");
                        String query2 = "DELETE FROM article WHERE articleno=?";
                        ps2=con.prepareStatement(query2);
                        ps2.setInt(1, idid);
                        ps2.executeUpdate();
                        System.out.println("delete successed");
                        return false;
            }
            else{
                System.out.println("the article that you inserted doesn't exist");
            }
        }
        catch (SQLException e) {
            throw new DaoException("deleteArticle(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteArticle(): " + e.getMessage());
            }
        }
        return true;
    }
    
    public boolean deleteArticlebyId(int id) throws DaoException{
        boolean yn = false;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try{
            con = getConnection();
                        String query2 = "DELETE FROM article WHERE articleno=?";
                        ps2=con.prepareStatement(query2);
                        ps2.setInt(1, id);
                        ps2.executeUpdate();
                        System.out.println("delete successed");
                        yn= true;
            }
            
        
        catch (SQLException e) {
            throw new DaoException("deleteArticle(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteArticle(): " + e.getMessage());
            }
        }
        return yn;
    }

    
            public article fineByID(int id) throws DaoException{
                article te = null;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try{
            con = getConnection();
            String query = "SELECT * FROM article WHERE articleno=?";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            if(rs.next()){
                int arcno = rs.getInt("articleno");
                String arctitile = rs.getString("articletitle");
                String arccontent = rs.getString("articlecontent");
                int userno = rs.getInt("userno");
                te = new article(arcno, arctitile, arccontent, userno);
            }
            
        }
        catch (SQLException e) {
            throw new DaoException("deleteArticle(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteArticle(): " + e.getMessage());
            }
        }
        return te;
    }
   //only for testing 
        public static void main(String [] args) throws DaoException{
        articleDao  dao = new articleDao();
        article temp = dao.createArticle("asdasd", "qweqweqwe", 12);
         temp = dao.createArticle("yousze", "vbnvbnvbnv", 12);
         temp = dao.createArticle("yousze", "asd", 12);
    }

    public ArrayList<comment> fineByID(article ac) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
