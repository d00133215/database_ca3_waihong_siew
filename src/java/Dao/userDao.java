/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Class.user;
import Exception.DaoException;
import Interface.userDaoInterface;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author d00133215
 */
public class userDao extends Dao implements userDaoInterface{
    
    
    public user findUserByUserNameAndPassword(String useremail, String password) throws DaoException{
             Connection con = null;
             PreparedStatement ps = null;
             ResultSet rs = null; 
             user u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM user WHERE useremail = ? AND userpassword = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, useremail);
            ps.setString(2, password);

            rs = ps.executeQuery();

            if (rs.next()) {
                int userId = rs.getInt("userno");
                String usersName = rs.getString("username");
                String userPassword = rs.getString("userpassword");
                String email = rs.getString("useremail");
                String userFirstName = rs.getString("firstname");
                String userLastName = rs.getString("lastname");
                u = new user(userId, usersName, userPassword, email,userFirstName, userLastName);
            }
            else{
            System.out.println("none");
            }

        } catch (SQLException e) {
            throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
            }
        }
        return u ;
    }
    
            public ArrayList<user> findUserByName(String word) throws DaoException{
                ArrayList<user> tempList = new ArrayList<user>();;
             Connection con = null;
             PreparedStatement ps = null;
             ResultSet rs = null; 
             user u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM user WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, word);

            rs = ps.executeQuery();

            while (rs.next()) {
                int userId = rs.getInt("userno");
                String usersName = rs.getString("username");
                String userPassword = rs.getString("userpassword");
                String email = rs.getString("useremail");
                String userFirstName = rs.getString("firstname");
                String userLastName = rs.getString("lastname");
                u = new user(userId, usersName, userPassword, email,userFirstName, userLastName);
                tempList.add(u);
            }
            

        } catch (SQLException e) {
            throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
            }
        }
        return tempList ;
    }
    public user findUserByEmail(String useremail) throws DaoException{
             Connection con = null;
             PreparedStatement ps = null;
             ResultSet rs = null; 
             user u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM user WHERE useremail = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, useremail);

            rs = ps.executeQuery();

            if (rs.next()) {
                int userId = rs.getInt("userno");
                String usersName = rs.getString("username");
                String userPassword = rs.getString("userpassword");
                String email = rs.getString("useremail");
                String userFirstName = rs.getString("firstname");
                String userLastName = rs.getString("lastname");
                u = new user(userId, usersName, userPassword, email,userFirstName, userLastName);
            }
            else{
            System.out.println("none");
            }

        } catch (SQLException e) {
            throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("findUserByUserNameAndPassword(): " + e.getMessage());
            }
        }
        return u ;
    }
    
     public user createUser(String userName1, String password1, String email, String fname, String lName) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        user u = null;
        try {
            con = getConnection();
            String query = "SELECT useremail FROM user WHERE useremail = ?";
            ps = con.prepareStatement(query);
            ps.setString(1,email);

            rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("user name " + userName1 + " already exists");
            }
            else{
            String command = "INSERT INTO user (username , userpassword , useremail, firstname , lastname) VALUES(?, ?, ?,?, ?)";
            ps = con.prepareStatement(command);
            ps.setString(1, userName1);
            ps.setString(2, password1);
            ps.setString(3, email);
            ps.setString(4, fname);
            ps.setString(5, lName);
            ps.executeUpdate();
                  
            System.out.println("user created");
            String command2 = "select userno from user where useremail = ?";
            ps2 = con.prepareStatement(command2);
            ps2.setString(1, email);
           
            rs = ps2.executeQuery();
            if (rs.next()) {
            int userId = rs.getInt("userno");
            u = new user(userId,userName1,password1,email,fname,lName);
            }
            }
        } catch (SQLException e) {
            throw new DaoException("createUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("createUser(): " + e.getMessage());
            }
        }
        return u;
    }
     public user editDetail(user old, String fname, String lname, String password) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        user u = null;
        try {
            con = getConnection();
            String query = "update user set firstname = ? , lastname = ?, userpassword = ? where useremail = ?";
            ps = con.prepareStatement(query);
            ps.setString(1,fname);
            ps.setString(2,lname);
            ps.setString(3,password);
            ps.setString(4,old.userEmail);

            ps.execute();
            String command2 = "select * from user where useremail = ?";
            ps2 = con.prepareStatement(command2);
            ps2.setString(1, old.userEmail);
           
            rs = ps2.executeQuery();
            if (rs.next()) {
            int userId = rs.getInt("userno");
            String userName1 = rs.getString("username");
            String password1 = rs.getString("userpassword");
            String email = rs.getString("useremail");
            String fName = rs.getString("firstname");
            String lName = rs.getString("lastname");
            u = new user(userId,userName1,password1,email,fName,lName);
            } 
        }
        catch (SQLException e) {
            throw new DaoException("editDetailDAO(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("editDetailDAO(): " + e.getMessage());
            }
        }
         
        return u;
     }
     
      public ArrayList<user> listAllUser() throws DaoException{
         Connection con = null;
            ArrayList<user> arc = new ArrayList<user>();
             PreparedStatement ps = null;
             ResultSet rs = null; 
             user u = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM user ";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
                int userNo = rs.getInt("userno");
                String userName = rs.getString("username");
                String userPassword = rs.getString("userpassword");
                String userEmail = rs.getString("useremail");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastname");
                u = new user(userNo, userName, userPassword, userEmail,firstName,lastName);
                arc.add(u);
            }

        } catch (SQLException e) {
            throw new DaoException("listAllUser(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("listAllUser(): " + e.getMessage());
            }
        }
        return arc ;
    }
    
    public void deleteUser(String email) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
         try{
            con = getConnection();
            String query = "SELECT * FROM user WHERE useremail=?";
            ps = con.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if(rs.next()){
                        int idid = rs.getInt("userno");
                        String query2 = "DELETE FROM user WHERE userno=?";
                        ps2=con.prepareStatement(query2);
                        ps2.setInt(1, idid);
                        ps2.executeUpdate();
                        System.out.println("delete successed");
                        
            }
            else{
                System.out.println("this user eamil doesnt exiest");
            }
        }
        catch (SQLException e) {
            throw new DaoException("deleteUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteUser(): " + e.getMessage());
            }
        }
    }
    
    public user fineUserById(int id) throws DaoException{
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        user us = null;
         try{
            con = getConnection();
            String query = "SELECT * FROM user WHERE userno=?";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
             if (rs.next()) {
                int userId = rs.getInt("userno");
                String usersName = rs.getString("username");
                String userPassword = rs.getString("userpassword");
                String email = rs.getString("useremail");
                String userFirstName = rs.getString("firstname");
                String userLastName = rs.getString("lastname");
                us = new user(userId, usersName, userPassword, email,userFirstName, userLastName);
            }
            else{
                System.out.println("this user id doesnt exiest");
            }
        }
        catch (SQLException e) {
            throw new DaoException("deleteUser(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("deleteUser(): " + e.getMessage());
            }
        }
      
        return us;
    }
    
    public static void main (String [] args) throws DaoException{
        userDao ud = new userDao();
        user temp = ud.fineUserById(1);
        System.out.println(temp);
    }
    
     
}
