/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dao;

import Class.article;
import Class.comment;
import Class.user;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author waihong
 */
public class commentDao extends Dao{
    
    public boolean createCommentinAr(int number, user us, String content) throws DaoException{
            
            Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        boolean u = false;
        try {
            con = getConnection();
            String query = "INSERT INTO comment (  articleno  , usersno , content ) VALUES( ?, ?,?)";
            ps = con.prepareStatement(query);
            ps.setInt(1,number);
            ps.setInt(2, us.getUserid());
            ps.setString(3,content);

            ps.executeUpdate();
            
            u=true;
        } catch (SQLException e) {
            throw new DaoException("articleCreate(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("articleCreate(): " + e.getMessage());
            }
        }
        return u;
    }
    
    public ArrayList<comment> ListAllCommentByArticleID(article u) throws DaoException{
        Connection con = null;
            ArrayList<comment> arc = new ArrayList<comment>();
             PreparedStatement ps = null;
             ResultSet rs = null; 
             comment qwe = null;
              try {
            con = getConnection();

            String query = "SELECT * FROM comment where articleno = ? ";
            ps = con.prepareStatement(query);
            ps.setInt(1, u.getArticleId());
            rs = ps.executeQuery();

            while(rs.next()) {
                int commentno = rs.getInt("commentno");
                int articleno = rs.getInt("articleno");
                int usersno = rs.getInt("usersno");
                Object date = rs.getObject("commentdate");
                String content = rs.getString("content");
                qwe = new comment(commentno,articleno,usersno,content);
                arc.add(qwe);
            }

        } catch (SQLException e) {
            throw new DaoException("listAllArticleCommand(): " + e.getMessage());
        }  finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("listAllArticleCommand(): " + e.getMessage());
            }
        }
        return arc ;
    }
    
    public static void main(String [] args) throws DaoException{
        commentDao dao = new commentDao();
        ArrayList<comment> temp = new ArrayList<comment>();
       temp = dao.ListAllCommentByArticleID(new article(2,"WaiHong" , "ASDASDADASDASDASDASDASDASDAasasdaSDAD", 1));
       
       for(int i = 0; i<temp.size();i++){
           System.out.println(temp.get(i));
       }
    }
}
