/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Class.user;
import Exception.DaoException;
/**
 *
 * @author d00133215
 */
public interface userDaoInterface {
   public user findUserByUserNameAndPassword(String userName, String password) throws DaoException;
   public user createUser(String userName1, String password1, String email, String fname, String lName) throws DaoException;
}
