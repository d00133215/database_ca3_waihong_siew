/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Class.article;
import Exception.DaoException;
import java.util.ArrayList;

/**
 *
 * @author d00133215
 */
public interface articleDaoInterface {
    public article createArticle(String articleTitle,String articleContent,int userNo) throws DaoException;
    public ArrayList<article> listAllArticle() throws DaoException;
    public void editArticleTitle(String name ,String newName) throws DaoException;
    public boolean deleteArticle(String name) throws DaoException;
}
