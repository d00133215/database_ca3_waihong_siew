/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.user;
import Services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class likePostCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/home.jsp";
        
         HttpSession session  = request.getSession();
         UserService userService = new UserService();
         
        String temp = request.getParameter("actn");
        int actNum = Integer.parseInt(temp);
        user users = (user) session.getAttribute("us1");
         boolean ynn= userService.likeAnPost(actNum, users);
         
        return forwardToJsp;
    }
    
}
