/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.user;
import Services.UserService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class seachingCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/followerpage.jsp";
        HttpSession session  = request.getSession();
        String temp = "Seaching result";
        String word = request.getParameter( "seachbox" );
        session.setAttribute("titileOf", temp);
         UserService userService = new UserService();
         ArrayList<user> listUser = new ArrayList<user>();
        if(word.equals("")){
            listUser = userService.listAllUser();
        }
        else{
          listUser = userService.seaching(word);
        }
         session.setAttribute("ListUserfi", null);
         session.setAttribute("ListUser", null);
         session.setAttribute("ssseee", listUser);
        return forwardToJsp;
    }
    
}
