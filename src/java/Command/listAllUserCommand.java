/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.article;
import Class.user;
import Services.AdminService;
import Services.UserService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class listAllUserCommand implements Command{

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session  = request.getSession();
        
        
        AdminService adminService = new AdminService();
        ArrayList<user> listAllUser = new ArrayList<user>();;
        try {
            listAllUser = adminService.listAllUser();
        } catch (SQLException ex) {
            Logger.getLogger(listAllUserCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        session.setAttribute("allUser", listAllUser);
        session.setAttribute("viewAllArticle", "false");
  
        return "/adminHome.jsp";
    }
    
}
