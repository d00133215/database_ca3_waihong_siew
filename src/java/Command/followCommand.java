/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.user;
import Services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class followCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/profile.jsp";
         int user1ID = Integer.parseInt(request.getParameter("userA"));
         int user2ID = Integer.parseInt(request.getParameter("userB"));
         HttpSession session  = request.getSession();
         UserService userService = new UserService();
            boolean foll = userService.follwing(user1ID,user2ID);
        return forwardToJsp;
    }
    
}
