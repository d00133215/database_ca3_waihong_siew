/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author d00133215
 */
public class CommandFactory {
    private static CommandFactory factory = null;
    
    private CommandFactory() {
    }
	
    public static synchronized CommandFactory getInstance()
    {
        if(factory == null)
        {
            factory = new CommandFactory();
        }
        return factory;
    }
    
    public synchronized Command createCommand(String commandStr) 
    {
    	Command command = null;
    	
	//Instantiate the required Command object...
    	if (commandStr.equals("Log In")) {
    		command = new UserLoginCommand();
    	}
        if (commandStr.equals("Sign up")){
                command = new userRegisterCommand();
        }
        if (commandStr.equals("listAllArticle")){
                command = new listAllArticleCommand();
        }
        if (commandStr.equals("post")){
                command = new postCommand();
        }
        if (commandStr.equals("logOut")){
                command = new logOutCommand();
        }
        if (commandStr.equals("Edit Profile")){
                command = new editProfileCommand();
        }
        if (commandStr.equals("adminLogOut")){
                command = new adminLogOutCommand();
        }
        if (commandStr.equals("log in as admin")){
                command = new adminLoginCommand();
        }
        if (commandStr.equals("listAllUser")){
                command = new listAllUserCommand();
        }
        if (commandStr.equals("adminListAllArticle")){
                command = new adminListAllArticleCommand();
        }
        if (commandStr.equals("deleteArticle")){
                command = new deleteArticleCommand();
        }
        if (commandStr.equals("AdminDeleteUser")){
                command = new AdminDeleteUserCommand();
        }
        if (commandStr.equals("uploadImages")){
                command = new uploadImagesCommand();
        }
        if (commandStr.equals("userProfile")){
                command = new userProfileCommand();
        }
        if (commandStr.equals("follow")){
                command = new followCommand();
        }
        if (commandStr.equals("viewFollowing")){
                command = new viewFollowingCommand();
        }
        if (commandStr.equals("viewFollower")){
                command = new viewFollowerCommand();
        }
        if (commandStr.equals("seaching")){
                command = new seachingCommand();
        }
        if (commandStr.equals("likePost")){
                command = new likePostCommand();
        }
        if (commandStr.equals("viewAC")){
                command = new viewACCommand();
        }
        
        if (commandStr.equals("posrAComment")){
                command = new posrACommentComment();
        }
        
        if (commandStr.equals("deleteUserArticle")){
                command = new deleteUserArticleCommand();
        }
        
        
        
        
        
        

        
    	//Return the instantiated Command object to the calling code...

    	return command;// may be null
    }    
}
