/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Class.admin;
import Services.AdminService;
import Services.UserService;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author d00133215
 */
public class adminLoginCommand implements Command{

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/admin.jsp";
        String name = request.getParameter( "admineamil" );
        String password = request.getParameter( "adminpassword" );
        
                        
                        HttpSession session  = request.getSession();

                        AdminService adminService = new AdminService();
                        admin adminLogin = null;
        try {
            adminLogin = adminService.login(name, password);
        } catch (SQLException ex) {
            Logger.getLogger(adminLoginCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
                            
                            if(adminLogin!=null){
                                session.setAttribute( "admin1", adminLogin );
                                forwardToJsp = "/adminHome.jsp";
                            }
                            else{
                                forwardToJsp = "/admin.jsp";                        
                                session.setAttribute( "yesNo", "true" );
                            }

      return forwardToJsp;
    }
    
}
