/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Class.user;
import Services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author d00133215
 */
public class editProfileCommand implements Command{

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = null;
        
        String firstname = request.getParameter( "inputfirstname" );
        String lastname = request.getParameter( "lastname" );
        String oldPassword = request.getParameter( "inputpassword" );
        String newPassword = request.getParameter( "Newinputpassword" );
        
        HttpSession session  = request.getSession();
        user tempUser = (user) session.getAttribute("us1");
        UserService userService = new UserService();
        user userLoggingIn = userService.login(tempUser.getUserEmail(), oldPassword);
        if(userLoggingIn==null){
            session.setAttribute("editSucess", "false");
            forwardToJsp = "/editprofile.jsp";
        }
        else{
            user userEdited = userService.editUser(tempUser,firstname,lastname,newPassword);
            if(userEdited!=null){
                session.setAttribute("us1", userEdited);
                forwardToJsp = "/home.jsp";
                
            }
        }
        
        return forwardToJsp;
    }
}
