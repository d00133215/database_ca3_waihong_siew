/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Class.article;
import Class.user;
import Services.UserService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author d00133215
 */
public class postCommand implements Command{
     public String execute(HttpServletRequest request, HttpServletResponse response) {
         String post = " none text ";
         post = request.getParameter("postArea");
         HttpSession session  = request.getSession();
         user users = (user) session.getAttribute("us1");
         UserService userService = new UserService();
         article userLoggingIn = userService.createArticle(users, post);
         
        user user = (user) session.getAttribute("us1");
        
        ArrayList<article> listAllArticle = userService.listAll(user);
        
        session.setAttribute("content", listAllArticle);
     return "/home.jsp";
     }
}
