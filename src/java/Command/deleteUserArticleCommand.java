/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.user;
import Services.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author waihong
 */
public class deleteUserArticleCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String profileid = request.getParameter("arId");
            int subjectid=Integer.parseInt(profileid);
            UserService userService = new UserService();
            boolean userP = userService.deleteAr(subjectid);
        return "/profile.jsp";
    }

}
