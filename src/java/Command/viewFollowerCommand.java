/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.article;
import Class.user;
import Services.UserService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class viewFollowerCommand implements Command{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
       String forwardToJsp = "/followerpage.jsp";
       HttpSession session  = request.getSession();
        String temp = "Your's Follower";
        session.setAttribute("titileOf", temp);
         user users = (user) session.getAttribute("us1");
         UserService userService = new UserService();
         ArrayList<user> listUser = userService.listAllFollwer(users);
         session.setAttribute("ListUser", listUser);
         session.setAttribute("ListUserfi", null);
        session.setAttribute("ssseee", null);
        return forwardToJsp;
    }
    
}
