/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.user;
import Services.AdminService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class AdminDeleteUserCommand implements Command{

   
    public String execute(HttpServletRequest request, HttpServletResponse response) {
         String content = request.getParameter("deleteUser");
         AdminService adminService = new AdminService();
        try {
            boolean deleteOrNot = adminService.deleteUser(content);
        } catch (SQLException ex) {
            Logger.getLogger(AdminDeleteUserCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
         HttpSession session  = request.getSession();
        
        
      //list all user again  
        ArrayList<user> listAllUser = new ArrayList<user>();
        try {
            listAllUser = adminService.listAllUser();
        } catch (SQLException ex) {
            Logger.getLogger(AdminDeleteUserCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        session.setAttribute("allUser", listAllUser);
        session.setAttribute("viewAllArticle", "false");
                     return "/adminHome.jsp";
    }
    
}
