/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.admin;
import Class.article;
import Class.user;
import Services.AdminService;
import Services.UserService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class deleteArticleCommand implements Command{
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String content = request.getParameter("deleteContent");
        AdminService adminService = new AdminService();
        try {
            boolean deleteOrNot = adminService.deleteArticle(content);
        } catch (SQLException ex) {
            Logger.getLogger(deleteArticleCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
                     
           
//list all article again 
                         UserService userService = new UserService();
        ArrayList<article> listAllArticle = userService.listAll();
        HttpSession session  = request.getSession();
        session.setAttribute("viewAllArticle",listAllArticle);
        session.setAttribute("allUser", "false");
                     
          return "/adminHome.jsp";
                     
    }
    
}
