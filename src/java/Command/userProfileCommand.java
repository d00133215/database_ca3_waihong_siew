/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Class.user;
import Services.UserService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author d00133215
 */
public class userProfileCommand implements Command{
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/index.jsp";
             HttpSession session  = request.getSession();
            String profileid = request.getParameter("usid");
            int subjectid=Integer.parseInt(profileid);
            UserService userService = new UserService();
            user userP = userService.userProfile(subjectid);
            session.setAttribute("us2", userP);
            
            user id = (user)session.getAttribute( "us1" );
            String qwq = null;
            boolean yesOrNoFollowed = userService.checkFollow(id.getUserid(),subjectid);
            if( yesOrNoFollowed == true){
            qwq = "true";
            }
            else{
             qwq = "false";
            }
            session.setAttribute("yesOrNo", qwq);
            //
            
            ArrayList<user> numtemp = userService.listAllFollwing(id);
            ArrayList<user> numtemp2 = userService.listAllFollwer(id);
            String paths = userService.getImagesById(subjectid);
            session.setAttribute("profileImages", paths);
            session.setAttribute("numberOfFollower", numtemp2);
            session.setAttribute("numberOfFollowing", numtemp);
            
            forwardToJsp = "/profile.jsp";
    return forwardToJsp;
    
    }
}
