/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Class.article;
import Class.user;
import Services.UserService;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author d00133215
 */
public class UserLoginCommand implements Command{
    
    public String execute(HttpServletRequest request, HttpServletResponse response) {
                        String forwardToJsp = null;
                        String name = request.getParameter( "email1122" );
                        String password = request.getParameter( "pass1122" );
                       
                        user user1=null;
                        
                        HttpSession session  = request.getSession();

                        UserService userService = new UserService();
                        user userLoggingIn = userService.login(name, password);
                        
                        String userImages = userService.getImages(userLoggingIn);
                        
                        ArrayList<article> listAllArticle = userService.listAll(userLoggingIn);
                         session.setAttribute("content", listAllArticle);
                        
                        if(userImages!=null){
                        session.setAttribute( "images", userImages );
                        }
                            
                            if(userLoggingIn!=null){
                                session.setAttribute( "us1", userLoggingIn );
                                forwardToJsp = "/home.jsp";
                            }
                            else{
                                forwardToJsp = "/index.jsp";                        
                                session.setAttribute( "yesNo", "true" );
                            }
                        

                        return forwardToJsp;
   }

}