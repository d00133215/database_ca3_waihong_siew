/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.article;
import Class.user;
import Services.UserService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author waihong
 */
public class adminListAllArticleCommand implements Command{

    
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session  = request.getSession();
        user user = (user) session.getAttribute("us1");
        
        UserService userService = new UserService();
        ArrayList<article> listAllArticle = userService.listAll(user);
        
        session.setAttribute("viewAllArticle",listAllArticle);
        session.setAttribute("allUser", "false");
        
    return "/adminHome.jsp";
    }
    
}
