/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author d00133215
 */
public interface Command {
    public String execute(HttpServletRequest request, HttpServletResponse response);
}
