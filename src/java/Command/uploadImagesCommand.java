/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Command;

import Class.images;
import Class.user;
import Services.UserService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.tomcat.util.http.fileupload.FileItem;

/**
 *
 * @author waihong
 */
public class uploadImagesCommand implements Command{

    
    
    public String execute(HttpServletRequest request, HttpServletResponse response){
        HttpSession session  = request.getSession();
        user tempUser = (user) session.getAttribute("us1");
        
        try{
            String description = request.getParameter("description"); // Retrieves <input type="text" name="description">
            Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">

            String filename = getFilename(filePart);
            InputStream filecontent = filePart.getInputStream();
            System.out.println(filePart); //display what is part
            String filePath = "E:\\Year3\\sem1\\project\\project1\\waihong_siew\\web\\images\\"+tempUser.getUserName()+filename; 
            FileOutputStream fos = new FileOutputStream(filePath);

            int inByte;

            while((inByte = filecontent.read()) != -1) fos.write(inByte);
            filecontent.close();
            fos.close();

            UserService userService = new UserService();
            String userLoggingIn = userService.createImg(tempUser, "images/"+tempUser.getUserName()+filename);

            String getImages = userService.getImages(tempUser);
            session.setAttribute("images",getImages);
        }
        catch(ServletException e){
            System.out.println("ServletException uploadImagesCommand: "+ e);
        }
        catch(IOException e){
            System.out.println("IOException uploadImagesCommand: "+ e);
        }
        
        
         
    String forwardToJsp ="/editprofile.jsp";
    
        return forwardToJsp;
    }
    
    public  String getFilename(Part part) {
    
    for (String cd : part.getHeader("content-disposition").split(";")) {
        if (cd.trim().startsWith("filename")) {
            String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
            return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
        }
    }
    
    return null;
}
    
}
