/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Class.user;
import Command.Command;
import Command.CommandFactory;
import Dao.userDao;
import Exception.DaoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/UserActionServlet"})
@MultipartConfig
public class UserActionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
       

        public UserActionServlet() {
            super(); 
        }
        
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            processRequest(request, response);
        }
        
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            
        processRequest(request, response);
	}
        
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Set the end destination as a backup, in case something goes wrong
        String forwardToJsp = "";

        //Check the 'action' parameter to see what the user wants...
        if ( request.getParameter("action") != null)
        {
            // Create a LoginCommand and execute it
            CommandFactory factory = CommandFactory.getInstance();
            Command command = factory.createCommand(request.getParameter("action"));
            // execute generated Command
            forwardToJsp = command.execute(request, response);
        }
       
        //Get the request dispatcher object and forward the request to the appropriate JSP page...
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(forwardToJsp);
        dispatcher.forward(request, response);
        
    
    }

}
