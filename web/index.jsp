<%-- 
    Document   : index
    Created on : 04-Nov-2013, 10:20:40
    Author     : d00133215
--%>

<%@page import="Class.user"%>
<%@page import="Dao.userDao"%>
<%@page import="Exception.DaoException"%>
<%@page import="java.sql.SQLException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wai's forum | Home</title>
       
        <script type="text/javascript" src="js/inputValidation.js"></script>
        <link rel="icon"  type="image/ico" href="images/favicon.ico" />
        
        <link rel="stylesheet" media="screen and (max-width: 600px)" href="Style/mobile.css" />
        <link rel="stylesheet" media="screen and (min-width: 600px)" href="Style/indexStyle.css" />
        
        <meta name="google-translate-customization" content="fa476946354dfc1c-509855dc0689212c-g7c1d1d96c0449677-f"></meta>
    </head>
    <body>
        <% 
                 
                        if( session.getAttribute( "yesNo" ) != null){
        %>  <p style="color:white;position:absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;top:60px; background-color:#ff6666;width:400px;height:50px;"><br/>password or email incorrect.</p><%
                        }
                        
                        if ( session.getAttribute( "userCreated" ) != null){
                        String temp = (String)session.getAttribute( "userCreated" );
                        
                            if(temp.equals("true")){  
                                 %>  <p style="color:white;position:absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;top:60px; background-color:#009999;width:400px;height:50px;"><br/>You have succuess registed, login pls</p><%   
                            }
                            if(temp.equals("false")){
                                %>  <p style="color:white;position:absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;top:60px; background-color:#ff6666;width:400px;height:50px;"><br/>User email had registed</p><%
                            }
                        }
                        
                   
 
            %>
                                
        <div id="container">
            <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<h1 id="mobile_sign_form"> Sign In</h1>
            <div id="login">
            <form id="login_form" name="login_form" action="UserActionServlet" method = "post">
                        <label>Email</label>
               
                        <input type="text" class="email1122" name="email1122" id="email1122" tabindex="1" />

                        <label>Password</label>
                
                        <input type="password" class="pass1122" name="pass1122" id="pass1122" tabindex="2" />
                        
                        
                        <input type="hidden" name="action" value="Log In" />
                        <input value="Log In" tabindex="4" type="submit" id="LoginUser" name="LoginUser" />
            </form>
            </div>      
<h1 id="mobile_sign_form"> Register</h1>
        
        <form id="signin_form" name="signin_form" action="UserActionServlet" onsubmit="return inputValidate()" method="post">
            <h1 id ="signUpTitle">Sign Up</h1>
            <table id="signUpTable" cellpadding="10">
     
                <tr><td>Name </td>
                    <td>
                    <input type="text" class="inputfirstname" name="inputfirstname" id="inputfirstname" placeholder="First Name" size="15"/><input type="text" class="lastname" name="lastname" id="lastname" placeholder="Last Name" size="15"/> 
                    </td>
                </tr>
                <tr>
                    <td>Email Address </td>
                    <td>
                   <input type="email" class="inputemail" name="inputemail" id="inputemail" placeholder="example@waihong.com" size="36"/>
                    </td>
                </tr>
                <tr>
                    <td>Password </td>
                    <td>
                      <input    type="password" class="inputpassword" name="inputpassword" id="inputpassword" size="36"/>
                     </td>
                </tr>
                <tr><td>Re-enter Password </td>
                    <td>
                     <input type="password" class="inputpassword2" name="inputpassword2" id="inputpassword2" size="36"/>
                    </td>
                </tr>
                <tr><td></td>
                    <td>
                  <input type="hidden" name="action" value="Sign up" />      
                  <input value="Sign up" tabindex="4" type="submit" id="RegisterUser" name="RegisterUser" />
                    </td>
                </tr>
            </table>
        </form>
        </div>
        <h4 style="color:rgb(48, 96, 136);margin-bottom:0px;">ABOUT US&nbsp; &nbsp; &nbsp;
                SUPPORT&nbsp; &nbsp; &nbsp;
                BLOG&nbsp; &nbsp; &nbsp;
                PRESS&nbsp; &nbsp; &nbsp;
                API&nbsp; &nbsp; &nbsp;
                JOBS&nbsp; &nbsp; &nbsp;
                PRIVACY&nbsp; &nbsp; &nbsp;
                TERMS&nbsp; &nbsp; &nbsp;
                &copy;WaiHong,Siew</h4>
       
    </body>
</html>
