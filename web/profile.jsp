<%-- 
    Document   : profile
    Created on : 2014-2-27, 23:01:58
    Author     : waihong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="Dao.imagesDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Dao.articleDao"%>
<%@page import="Class.article"%>
<%@page import="Class.user"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>welcome to our website</title>
        <link rel="stylesheet" type="text/css" href="Style/ProfileStyle.css">
        
                <script type="text/javascript"> 

   var myVar = setInterval(function(){loadXMLDoc()},1000);         
function loadXMLDoc()
{
var xmlhttp;

if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest();
  }
else
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4)
    {
        //document.getElementById("err").style.color="red";
        document.getElementById("ajaxcontent").innerHTML=xmlhttp.responseText;
 
    }
  }
xmlhttp.open("POST","profilecontent.jsp",true);
xmlhttp.send();
}


</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">// <![CDATA[
$(document).ready(function() {
$.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
setInterval(function() {
$('#ajaxcontent').load('profilecontent.jsp');
$('#post31').load('follower.jsp');
$('#post41').load('following.jsp');
}, 3000);

$("button").click(function(){
    $.ajax({url:"followbutton.jsp",success:function(result){
      $("#follBut").html(result);
    }});
  });

  });
// the "3000" here refers to the time to refresh the div.  it is in milliseconds. 



// ]]></script>
    </head>
    <body>
         <% 
                user ad2 = (user)session.getAttribute( "us1" );
                if(ad2 == null){
                    response.sendRedirect("index.jsp");
                    return;
                }
                String paths = (String)session.getAttribute("images");
                 String temp = null;
                 String foolbut = "visible";
                user ad1 = (user)session.getAttribute( "us2" );
                if(!(ad1.getUserEmail().equals(ad2.getUserEmail()))){
                paths = (String)session.getAttribute("profileImages");
                   
                
                String asssa = (String)session.getAttribute( "yesOrNo" );
                
                        if(asssa.equals("true")){
                            temp = "Followed";
                        }
                        else{
                        temp = "Follow";
                        }
               }
                else{
                    ad1=ad2;
                    foolbut="hidden";
                }
                ArrayList<user> follower = new ArrayList<user>();
                ArrayList<user> following = new ArrayList<user>();
                follower = (ArrayList<user>)session.getAttribute("numberOfFollower");
                following = (ArrayList<user>)session.getAttribute("numberOfFollowing");
                int followingNumber= 0;
                int followerNumber= 0;
                 followingNumber  = following.size();
                 followerNumber  = follower.size();
                
		%>
        <div id="top-bar" ></div>
        <div id="container">
       
                <div id="banner">
                    <div id="logo">
                    <img src="images/logo_1.png" id="imglogo" />  
                    </div>
                    <div id="seache">
                       <form id="qqq" name="qqq" action="UserActionServlet" method="post">
                        <input type="text" id="seachbox" name="seachbox"/>
                        <input type="hidden" name="action" value="seaching" />      
                        <input value="seaching" tabindex="4" type="submit" id="sebutton" name="sebutton" style="color:transparent;"/>
                         </form>
                    </div>
                     <div id="sttp">
                         
                         <p><%= ad2.getFirstName() %>
                             <span>|</span>
                             <a href="home.jsp">Home</a>
                        <span>|</span>
                        <a href="UserActionServlet?action=logOut">Log out</a></p>
                    </div>
                                   
                </div>
        </div>
                             <div id="container2" >
                        <div id="content123"><img id="profilepicture" name="profilepicture" src="<%=paths%>" style="position:absolute;margin-top:200px;margin-left:3px;"/> 
                            <div id="profiletop" style="background-image:url('');background: linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0.55));">
                                <button id="follBut" style="visibility:<%=foolbut%>;position:absolute;;margin-top:200px;margin-left:600px;"><%=temp%></button>
                            </div>
                    <div id="content"> 
                        <div id="postcontain">
                            <div id="post1"> &nbsp;&nbsp;</div>
                            <div id="post2"><%=ad1.getFirstName() %></div>
                            <div id="post3"><p>&nbsp;&nbsp;Following</p></div>
                                <div id="post4"><p>Followers</p></div>
                        </div>
                            <div id="postcontain">
                                &nbsp;&nbsp;&nbsp;<div id="post31">&nbsp;&nbsp;&nbsp;Loading...</div>
                                <div id="post41">Loading...</div>
                            </div>
                        <div id="line" ></div>
                        <div id="ajaxcontent">
                        
                    
                        </div>      
                </div>
                        </div>
               
        </div> <div id="footer">ABOUT US&nbsp; &nbsp; &nbsp;
                SUPPORT&nbsp; &nbsp; &nbsp;
                BLOG&nbsp; &nbsp; &nbsp;
                PRESS&nbsp; &nbsp; &nbsp;
                API&nbsp; &nbsp; &nbsp;
                JOBS&nbsp; &nbsp; &nbsp;
                PRIVACY&nbsp; &nbsp; &nbsp;
                TERMS&nbsp; &nbsp; &nbsp;
                &copy;WaiHong,Siew
        </div>
    </body>
</html>
</html>
