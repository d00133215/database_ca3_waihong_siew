<%-- 
    Document   : home
    Created on : 2013-11-5, 3:45:47
    Author     : waihong
--%>

<%@page import="Dao.imagesDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Dao.articleDao"%>
<%@page import="Class.article"%>
<%@page import="Class.user"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon"  type="image/ico" href="images/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>welcome to our website</title>
        <link rel="stylesheet" type="text/css" href="Style/homeStyle.css">
        <script type="text/javascript"> 

   var myVar = setInterval(function(){loadXMLDoc()},1000);         
function loadXMLDoc()
{
var xmlhttp;

if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest();
  }
else
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4)
    {
        //document.getElementById("err").style.color="red";
        document.getElementById("ajaxcontent").innerHTML=xmlhttp.responseText;
 
    }
  }
xmlhttp.open("POST","content.jsp",true);
xmlhttp.send();
}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script type="text/javascript">// <![CDATA[
$(document).ready(function() {
$.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
setInterval(function() {
$('#ajaxcontent').load('content.jsp');
$('#post31').load('follower.jsp');
$('#post41').load('following.jsp');
}, 3000); // the "3000" here refers to the time to refresh the div.  it is in milliseconds. 

});

// ]]></script>
            

    </head>
    <body onload="loadXMLDoc()">
         <% 

              
            
             
                user ad2 = (user)session.getAttribute( "us1" );
                session.setAttribute("us2", ad2);
                if(ad2 == null){
                    response.sendRedirect("index.jsp");
                    return;
                }
                String paths = (String)session.getAttribute("images");
		%>
        <div id="top-bar" ></div>
        <div id="container">
       
                <div id="banner">
                    <div id="logo">
                    <img src="images/logo_1.png" id="imglogo" />  
                    </div>
                    <div id="seache">
                         <form id="qqq" name="qqq" action="UserActionServlet" method="post">
                        <input type="text" id="seachbox" name="seachbox"/>
                        <input type="hidden" name="action" value="seaching" />      
                        <input value="seaching" tabindex="4" type="submit" id="sebutton" name="sebutton" style="color:transparent;"/>
                         </form>
                    </div>
                     <div id="sttp">
                         
                         <p><a href="UserActionServlet?action=userProfile&usid=<%=ad2.getUserid()%>"><%=ad2.getFirstName() %></a>
                             <span>|</span>
                             Home
                        <span>|</span>
                        <a href="UserActionServlet?action=logOut">Log out</a></p>
                    </div>
                                   
                </div>
                        <div id="content123">
                 <div id="nav">
                     
                     <img id="profilepicture" name="profilepicture" src="<%=paths%>"/> 
                     
                     <div id="foll">
                         <div id="Following"><p>&nbsp;&nbsp;Following</p></div>
                         <div id="Followers"><p>Followers</p></div>
                         <div id="Following"><div id="post31">&nbsp;&nbsp;&nbsp;&nbsp;Loading..</div></div>
                         <div id="Followers"><div id="post41" >Loading...</div></div>
                         </div>
                    <ul id="links">
                        <li><a href="#" onclick="loadXMLDoc()" id="newfeedlink" name="newfeedlink">New Feed</a>
                        <li><a href ="UserActionServlet?action=userProfile&usid=<%=ad2.getUserid()%>">View Profile</a></li>
                        <li><a href ="editprofile.jsp">Edit</a></li>
                        <li><a href ="UserActionServlet?action=viewFollower">Following</a></li>
                        <li><a href ="UserActionServlet?action=viewFollowing">Followers</a></li>
                        
                    </ul>
                </div> 
                    <div id="content"> 
                        <div id="postcontain">
                <form id="postForm" name="postForm" action="UserActionServlet" method = "post">
                        <p>What is in your mind?</p>
                        
                        <textarea rows="3" cols="85"  name="postArea" id="postArea" class="postArea"></textarea>
                        
                        <input type="hidden" name="action" value="post" />
                        <input value="post" tabindex="4" type="submit" id="post" name="post" />
                        
                </form>
                        </div>
                        <div id="line" ></div>
                        <div id="ajaxcontent">
                        
                    
                        </div>      
                </div>
                        </div>
               
        </div> <div id="footer">ABOUT US&nbsp; &nbsp; &nbsp;
                SUPPORT&nbsp; &nbsp; &nbsp;
                BLOG&nbsp; &nbsp; &nbsp;
                PRESS&nbsp; &nbsp; &nbsp;
                API&nbsp; &nbsp; &nbsp;
                JOBS&nbsp; &nbsp; &nbsp;
                PRIVACY&nbsp; &nbsp; &nbsp;
                TERMS&nbsp; &nbsp; &nbsp;
                &copy;WaiHong,Siew
        </div>
    </body>
</html>
