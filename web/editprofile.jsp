<%-- 
    Document   : editprofile
    Created on : 2013-11-5, 3:45:47
    Author     : waihong
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Dao.articleDao"%>
<%@page import="Class.article"%>
<%@page import="Class.user"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>welcome to our website</title>
        <link rel="stylesheet" type="text/css" href="Style/homeStyle.css">
        <link rel="stylesheet" type="text/css" href="Style/edit.css">
        <script type="text/javascript" src="js/inputValidation.js"></script>
        <link rel="icon"  type="image/ico" href="images/favicon.ico" />
    </head>
    <body>
         <% 
                user ad2 = (user)session.getAttribute( "us1" );
                if(ad2 == null){
                    response.sendRedirect("index.jsp");
                    return;
                }
                if(session.getAttribute("editSucess")!=null ){
    
                %>  <div style="text-align: center;"><p style="color:white;position:absolute;margin-left: auto;margin-right: auto;left: 0;right: 0;top:550px; background-color:#ff6666;width:400px;height:50px;"><br/> Edit details fail, because old password incorrect.</p></div><%
                }
                String paths = (String)session.getAttribute("images");
		%>
         <div id="top-bar" ></div>
        <div id="container">
       
                <div id="banner">
                    <div id="logo">
                    <img src="images/logo_1.png" id="imglogo" />  
                    </div>
                    <div id="seache">
                        <form id="qqq" name="qqq" action="UserActionServlet" method="post">
                        <input type="text" id="seachbox" name="seachbox"/>
                        <input type="hidden" name="action" value="seaching" />      
                        <input value="seaching" tabindex="4" type="submit" id="sebutton" name="sebutton" style="color:transparent;"/>
                         </form>
                    </div>
                     <div id="sttp">
                         
                         <p><a href="UserActionServlet?action=userProfile&usid=<%=ad2.getUserid()%>"><%=ad2.getFirstName() %></a>
                             <span>|</span>
                             Home
                        <span>|</span>
                        <a href="UserActionServlet?action=logOut">Log out</a></p>
                    </div>
                                   
                </div>
                        <div id="content123">
                 <div id="nav">
                     
                     <img id="profilepicture" name="profilepicture" src="<%=paths%>"/> 
                     
                     <div id="foll">
                         <div id="Following"><p>&nbsp;&nbsp;Following</p>&nbsp;&nbsp;&nbsp;3</div>
                         <div id="Followers"><p>Followers</p>3</div>
                         </div>
                    <ul id="links">
                        <li><a href ="home.jsp" name="newfeedlink" id="newfeedlink">New Feed</a></li>
                        <li><a href ="UserActionServlet?action=userProfile&usid=<%=ad2.getUserid()%>">View Profile</a></li>
                        <li><a href ="editprofile.jsp">Edit</a></li>
                        <li><a href ="UserActionServlet?action=viewFollowing">Following</a></li>
                        <li><a href ="UserActionServlet?action=viewFollower">Followers</a></li>
                        
                    </ul>
                </div> 
                    <div id="content"> 
              <h1 id ="signUpTitle" style="margin-left:20px;"> Edit your Profile</h1>
            <div id="line"></div>          
         <form id="signin_form" name="signin_form" action="UserActionServlet" onsubmit="return inputValidator3()" method="post">
            
            
            <table id="signUpTable" cellpadding="10">
                
                <tr><td>Name </td>
                    <td>
                    <input type="text" class="inputfirstname" name="inputfirstname" id="inputfirstname" value="<%=ad2.getFirstName()%>" size="15"/><input type="text" class="lastname" name="lastname" id="lastname" value="<%=ad2.getLastName()%>" size="15"/> 
                    </td>
                </tr>
                <tr>
                    <td>Email Address (unable to change email)</td>
                    <td>
                   <input type="email" class="inputemail" name="inputemail" id="inputemail" Value="<%=ad2.getUserEmail()%>" readonly="readonly" size="36"/>
                    </td>
                </tr>
                <tr>
                     <td>Old Password(insert password for information edit) </td>
                    <td>
                      <input type="password" class="inputpassword" name="inputpassword" id="inputpassword" size="36"/>
                     </td>
                </tr>
                <tr>
                    <td>New Password(leave it blank if you don't want change) </td>
                    <td>
                      <input type="password" class="Newinputpassword" name="Newinputpassword" id="Newinputpassword" size="36"/>
                     </td>
                </tr>
                <tr><td>Re-enter Password(leave it blank if you don't want change) </td>
                    <td>
                     <input type="password" class="ReNewinputpassword" name="ReNewinputpassword" id="ReNewinputpassword" size="36"/>
                    </td>
                </tr>
                <tr><td></td>
                    <td>
                  <input type="hidden" name="action" value="Edit Profile" />      
                  <input value="Edit Profile" tabindex="4" type="submit" id="RegisterUser" name="RegisterUser" />
                    </td>
                </tr>
            </table>
        </form>
                    <div id="line"></div>
                    <form action="UserActionServlet" method="post" enctype="multipart/form-data" style="margin-left:10px;">
                        <table> 
                        
                            <tr>
                                <td>upload a images for your profile</td><td>
                            <input type="file" name="file" />
                            <input type="hidden" name="action" value="uploadImages" >
                            <input type="submit" value="uploadImages" id="imgupload"/>
                                </td>
                        </tr>
                        </table>
                        </form>
  
                </div></div>
                <div id="footer">ABOUT US&nbsp; &nbsp; &nbsp;
                SUPPORT&nbsp; &nbsp; &nbsp;
                BLOG&nbsp; &nbsp; &nbsp;
                PRESS&nbsp; &nbsp; &nbsp;
                API&nbsp; &nbsp; &nbsp;
                JOBS&nbsp; &nbsp; &nbsp;
                PRIVACY&nbsp; &nbsp; &nbsp;
                TERMS&nbsp; &nbsp; &nbsp;
                &copy;WaiHong,Siew
        
        </div>
    </body>
</html>
