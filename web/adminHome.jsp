<%-- 
    Document   : adminHome
    Created on : 2013-11-5, 3:46:23
    Author     : waihong
--%>

<%@page import="Class.article"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Class.user"%>
<%@page import="Class.admin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon"  type="image/ico" href="images/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to our control panal</title>
        <link rel="stylesheet" type="text/css" href="Style/homeStyle.css">
    </head>
    <body>
         <% 
                admin ad2 = (admin)session.getAttribute( "admin1" );
                if(ad2==null){
                    response.sendRedirect("admin.jsp");
                    return;
                }
		%>
        <div id="container">
                <div id="banner">
                     <div id="sttp">
                        <%= ad2.getAmdinName()%>
                        <span>|</span>
                        <a href="UserActionServlet?action=adminLogOut" style="color:black;">Log out</a>
                    </div>
                    <div id="logo"><img src="images/logo.png" id="logoImages"/></div>
                   
                </div>
                <div id="nav">
                    <img id="profilepicture" name="profilepicture" src="images/Admin.png"/>
                    <h4>Admin Function</h4>
                    <ul>
                        <li><a href="UserActionServlet?action=listAllUser">List All Users</a></li>
                        <li><a href="UserActionServlet?action=adminListAllArticle">List All Article</a></li>
                    </ul>
                </div>
                <div id="content">
                    
                        <%
                        if(session.getAttribute("allUser")!=null&&!(session.getAttribute("allUser").equals("false"))){
                            ArrayList<user> temp = (ArrayList<user>)session.getAttribute( "allUser" );
                            for(int i = temp.size()-1; i>=0 ; i--){
                                %>
                                <form action="UserActionServlet">
                                <ul>
                                    <li Style="color:#3366CC"><%=temp.get(i).getFirstName() + " "+temp.get(i).getLastName() %></li>
                                    <li><%=temp.get(i).getUserEmail() %> </li>
                                    </ul>
                              <input type="hidden" name="deleteUser" id="deleteUser" class="deleteUser"  value="<%=temp.get(i).getUserEmail()%>"/>
                              <input type="hidden" name="action" value="AdminDeleteUser" />
                              <input value="AdminDeleteUser" tabindex="4" type="submit" id="deleteButton" name="deleteButton" />
                                
                                </form>
                                <div id="line" ></div>
                                <%
                            }}
                         if(session.getAttribute("viewAllArticle")!=null&&!(session.getAttribute("viewAllArticle").equals("false"))){
                            
                            ArrayList<article> temp = (ArrayList<article>)session.getAttribute( "viewAllArticle" );
                            for(int i = temp.size()-1; i>=0 ; i--){
                                %>
                                <form action="UserActionServlet">
                             <ul>
                                    <li Style="color:#3366CC"><%=temp.get(i).getTitle()%></li>
                                    <li><%=temp.get(i).getContent()%></li>
                             </ul>
                             
                             <input type="hidden" name="deleteContent" id="deleteContent" class="deleteContent"  value="<%=temp.get(i).getContent()%>"/>
                             <input type="hidden" name="action" value="deleteArticle" />
                            <input value="deleteArticle" tabindex="4" type="submit" id="deleteButton" name="deleteButton" />
                            </form>
                                <div id="line" ></div>
                                <%
                            }}
                        
                    %>
                    
                </div>
                <div id="footer">created by waihong</div>
        </div>
    </body>
</html>
