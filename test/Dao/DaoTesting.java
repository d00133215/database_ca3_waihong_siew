package Dao;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import Class.admin;
import Class.user;
import Exception.DaoException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Owner
 */
public class DaoTesting {
    
    public DaoTesting() {
    }
    
    @Test
    public void testCal() throws DaoException {
        System.out.print("Java testing start");
        
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
        @Test
    public void testCal1() throws DaoException {
        System.out.print("Java testing start");
        userDao instance = new userDao();
        user temp = instance.findUserByUserNameAndPassword("waihong@gmail.com", "qwe");
        user correct = new user(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the user will match ",temp,correct);
       
    }
        @Test
    public void testCal2() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        userDao instance = new userDao();
        user temp = instance.findUserByUserNameAndPassword("waihong@gmail.com", "qwe");
        user correct = new user(2,"thefacebook","qwe","thefacebok@gmail.com","facebook","the");
        
       assertEquals("the user will match ",temp,correct);
       
    }

     @Test
    public void testCal3() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
     @Test
    public void testCal4() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
     @Test
    public void testCal5() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
     @Test
    public void testCal6() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
     @Test
    public void testCal7() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
     @Test
    public void testCal8() throws DaoException {
        System.out.print("Java testing start");
        double DELTA = 1e-15;
        adminDao instance = new adminDao();
        admin temp = instance.fineAdminByUserNameAndPassword("waihong@gmail.com", "qwe");
        admin correct = new admin(1,"waihong","qwe","waihong@gmail.com","waihong","siew");
        
       assertEquals("the admin will match ",temp,correct);
       
    }
}
